Open Domain provides a YAML descriptor file format for describing a Domain Driven Design (DDD) strategic and tactical model.
