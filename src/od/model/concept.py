class Concept:

    def __init__(self, info):
        self.term = info.get('term')
        self.definition = info.get('definition')

    def __repr__(self):
        return f'Concept[term={self.term}, definition={self.definition}]'
