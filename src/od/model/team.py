class Team:

    def __init__(self, info):
        self.name = info.get('name')
        self.product_owner = info.get('product_owner')
        self.contact = info.get('contact')
        self.website = info.get('website')
