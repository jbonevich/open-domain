from od.model.model import ModularBase, ModelType


class Factory(ModularBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.FACTORY, module)

    def __repr__(self):
        return f'Factory[{self.name}]'
