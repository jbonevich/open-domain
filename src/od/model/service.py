from od.model.model import ConceptualBase, ModelType


class Service(ConceptualBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.DOMAIN_SERVICE, module)

    def __repr__(self):
        return f'Service[{self.name}]'
