from od.model.attribute import Attribute


class ModelType:

    AGGREGATE = 'Aggregate'
    DOMAIN_EVENT = 'Domain Event'
    ENTITY = 'Entity'
    FACTORY = 'Factory'
    MODULE = 'Module'
    REPOSITORY = 'Repository'
    DOMAIN_SERVICE = 'Domain Service'
    VALUE_OBJECT = 'Value Object'


class ModelBase:

    def __init__(self, info, model_type, bounded_context):
        self.name = info.get('name')
        self.type = model_type
        self.bounded_context = bounded_context


class ModularBase(ModelBase):

    def __init__(self, info, model_type, module):
        super().__init__(info, model_type, module.bounded_context)
        self.module = module


class ConceptualBase(ModularBase):

    def __init__(self, info, model_type, module, supports_attributes=False):
        super().__init__(info, model_type, module)
        concept = info.get('concept')
        self.concept = module.bounded_context.get_domain_concept(concept)
        self.supports_attributes = supports_attributes

        if supports_attributes:
            self.attributes = []
            attributes = info.get_all('attributes')
            for attr in attributes:
                self.attributes.append(Attribute(attr))
