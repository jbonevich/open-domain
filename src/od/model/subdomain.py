from od.model.concept import Concept


class Subdomain:
    CORE = 'Core'
    SUPPORT = 'Support'
    GENERIC = 'Generic'

    def __init__(self, info, classification=None):
        self.name = info.get('name')
        self.classification = classification or info.get('classification')
        self.vision = info.get('vision')

        self.description = []
        for desc in info.get_all('description'):
            self.description.append(desc.get('paragraph'))

        self.domain_concepts = []
        for concept in info.get_all('domain_concepts'):
            self.domain_concepts.append(Concept(concept))

    def get_domain_concept(self, term):
        for concept in self.domain_concepts:
            if term == concept.term:
                return concept

    def __repr__(self):
        return f'Subdomain[name={self.name}, classification={self.classification}, vision=\'{self.vision}\']'
