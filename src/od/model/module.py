from od.model.aggregate import Aggregate
from od.model.entity import Entity
from od.model.event import Event
from od.model.factory import Factory
from od.model.model import ModelBase, ModelType
from od.model.repository import Repository
from od.model.service import Service
from od.model.valueobject import ValueObject


def _create_full_module_name(base_package, module_name):
    if base_package:
        return f'{base_package}.{module_name}'
    return module_name


class Module(ModelBase):

    def __init__(self, info, bounded_context, base_package):
        super().__init__(info, ModelType.MODULE, bounded_context)
        self.full_name = _create_full_module_name(base_package, self.name)

        concept = info.get('concept')
        self.concept = bounded_context.get_domain_concept(concept)

        self.modules = []
        modules = info.get_all('modules')
        for mod in modules:
            _module = Module(mod, bounded_context, self.full_name)
            self.modules.append(_module)
            bounded_context.add_sub_module(_module)

        self.entities = self._build_models(info.get_all('entities'), Entity)
        self.domain_events = self._build_models(info.get_all('domain_events'), Event)
        self.value_objects = self._build_models(info.get_all('value_objects'), ValueObject)
        self.aggregates = self._build_models(info.get_all('aggregates'), Aggregate)
        self.repositories = self._build_models(info.get_all('repositories'), Repository)
        self.factories = self._build_models(info.get_all('factories'), Factory)
        self.services = self._build_models(info.get_all('services'), Service)

        bounded_context.add_model_object(self)

    def _build_models(self, models, model_class):
        model_collection = []
        for model in models:
            _model = model_class(model, self)
            model_collection.append(_model)
            self.bounded_context.add_model_object(_model)
        return model_collection

    def __repr__(self):
        return f'Module[{self.full_name}]'
