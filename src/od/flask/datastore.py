from flask import _app_ctx_stack


class DataStore:

    def __init__(self):
        self.domain = None
        self.resource_dir = None

    def init_app(self, app, domain, resource_dir):
        self.domain = domain
        self.resource_dir = resource_dir
        app.teardown_appcontext(self.teardown)

    def teardown(self, exception):
        ctx = _app_ctx_stack.top
        if hasattr(ctx, 'datastore'):
            ctx.datastore = None

    def get(self):
        ctx = _app_ctx_stack.top
        if ctx is not None:
            if not hasattr(ctx, 'datastore'):
                ctx.datastore = self
            return self


ds = DataStore()
