from od.model.subdomain import Subdomain
from od.model.contextmap import ContextMap
from od.model.team import Team


class CoreDomainChart:

    def __init__(self, info):
        self.filename = info.get('filename')
        self.width = info.get('width')
        self.height = info.get('height')


class Domain:

    def __init__(self, info):
        self.name = info.get('name')
        self.published_version = info.get('published_version')
        self.copyright = info.get('copyright')
        self.contact_email = info.get('contact')
        self.core_domain = Subdomain(info.get('core'), Subdomain.CORE)

        self.description = []
        for desc in info.get_all('description'):
            self.description.append(desc.get('paragraph'))

        self.subdomains = []
        for sub in info.get_all('subdomains'):
            self.subdomains.append(Subdomain(sub))

        self.teams = []
        for team in info.get_all('teams'):
            self.teams.append(Team(team))

        self.context_map = ContextMap(info.get('context_map'), self)

        image = info.get('core_domain_chart')
        if image:
            self.core_domain_chart = CoreDomainChart(image)

    def get_subdomain(self, name):
        if self.core_domain.name == name:
            return self.core_domain
        for sub in self.subdomains:
            if sub.name == name:
                return sub
        return None

    def get_bounded_context(self, name):
        for bc in self.context_map.bounded_contexts:
            if bc.name == name:
                return bc
        return None

    def get_team(self, name):
        for team in self.teams:
            if team.name == name:
                return team
        return None

    def __repr__(self):
        return f'Domain[name={self.name}, version={self.published_version}]'
