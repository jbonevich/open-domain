from od.model.boundedcontext import BoundedContext
from od.model.relationship import RelationshipFactory


class _ContextMapDiagram:

    def __init__(self, info):
        self.filename = info.get('filename')
        self.width = info.get('width')
        self.height = info.get('height')


class ContextMap:

    def __init__(self, info, root_domain):
        self.bounded_contexts = []
        contexts = {}
        for bc in info.get_all('bounded_contexts'):
            ctx = BoundedContext(bc, root_domain)
            self.bounded_contexts.append(ctx)
            contexts[ctx.name] = ctx

        image = info.get('context_map_diagram')
        if image:
            self.context_map_diagram = _ContextMapDiagram(image)

        self.relationships = []
        relationships = info.get_all('relationships')
        if relationships:
            for rel in relationships:
                relationship = RelationshipFactory.create_relationship(rel)
                for participant in relationship.participants:
                    bc = contexts[participant.bounded_context_name]
                    bc.add_relationship(relationship)
                self.relationships.append(relationship)
