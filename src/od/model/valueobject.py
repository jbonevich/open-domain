from od.model.attribute import Attribute
from od.model.model import ConceptualBase, ModelType


class ValueObject(ConceptualBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.VALUE_OBJECT, module, supports_attributes=True)

        self.attributes = []
        attributes = info.get_all('attributes')
        for attr in attributes:
            self.attributes.append(Attribute(attr))

    def __repr__(self):
        return f'ValueObject[{self.name}]'
