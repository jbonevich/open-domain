import io
import yaml

from od.model.domain import Domain
from od.flask.app import FlaskApp
from od.utils import Dict
from pathlib import Path


class ServeCommand:

    @staticmethod
    def execute(args):
        inputfile = args.input
        path = Path(inputfile).resolve()
        parentdir = path.parent.absolute()
        print(f'Serving OpenDomain file {path.absolute()}')
        print(f'Setting domain resource directory to {parentdir}')
        debug = args.debug or False
        host = '0.0.0.0'
        port = 9876
        data = yaml.load(path.open(mode='r'), Loader=yaml.Loader)
        domain = Domain(Dict(data.get('domain')))
        app = FlaskApp(host=host, port=port, domain=domain, resource_dir=parentdir, debug=debug)
        app.run()
