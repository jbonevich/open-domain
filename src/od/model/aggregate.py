from od.model.model import ConceptualBase, ModelType


class Aggregate(ConceptualBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.AGGREGATE, module, supports_attributes=True)

    def __repr__(self):
        return f'Aggregate[{self.name}]'
