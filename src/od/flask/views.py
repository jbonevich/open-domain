from flask import Blueprint, render_template, abort, send_from_directory
from od.flask.datastore import ds

blueprint = Blueprint('views', __name__)


@blueprint.route('/')
def index():
    _domain = ds.get().domain
    return render_template('views/index.html', domain=_domain)


@blueprint.route('/about')
def about():
    _domain = ds.get().domain
    return render_template('views/about.html', domain=_domain)


@blueprint.route('/domain/<string:name>')
def domain(name):
    _domain = ds.get().domain
    _subdomain = _domain.get_subdomain(name)
    if _subdomain is None:
        abort(404, '')
    return render_template('views/domain.html', domain=_domain, subdomain=_subdomain)


@blueprint.route('/boundedcontext/<string:name>')
def bounded_context(name):
    _domain = ds.get().domain
    _bc = _domain.get_bounded_context(name)
    if _bc is None:
        abort(404, '')
    return render_template('views/bounded_context.html', domain=_domain, bc=_bc)


@blueprint.route('/boundedcontext/<string:bounded_context_name>/model/<string:model_name>')
def model_object(bounded_context_name, model_name):
    _domain = ds.get().domain
    _bc = _domain.get_bounded_context(bounded_context_name)
    if _bc is None:
        abort(404, 'No such bounded context')
    _model = _bc.get_model_object(model_name)
    if _model is None:
        abort(404, 'No such model object')
    return render_template('views/model.html', domain=_domain, bc=_bc, model=_model)


@blueprint.route('/team/<string:name>')
def team(name):
    _domain = ds.get().domain
    _team = _domain.get_team(name)
    if _team is None:
        abort(404, '')
    return render_template('views/team.html', domain=_domain, team=_team)


@blueprint.route('/images/<path:filename>')
def image(filename):
    _resource_dir = ds.get().resource_dir
    return send_from_directory(_resource_dir, filename)


@blueprint.add_app_template_filter
def indefinite(phrase):
    if phrase:
        if phrase[0] in 'AEIOUaeiou':
            return f'an {phrase}'
        return f'a {phrase}'
    return ''
