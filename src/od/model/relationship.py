class _RelationshipType:

    PARTNERSHIP = 'Partnership'
    SHARED_KERNEL = 'Shared Kernel'
    CUSTOMER_SUPPLIER = 'Customer/Supplier'
    SEPARATE_WAYS = 'Separate Ways'


class _Role:

    def __init__(self, name, role_type, article):
        self.name = name
        self.type = role_type
        self.article = article

    def is_upstream(self):
        return self.type == _RoleTypes.UPSTREAM.name

    def phrase(self):
        return f'Is {self.article} {self.name} {self.type.relation}'

    def __repr__(self):
        return f'Role[{self.name}]'


class _RoleType:

    def __init__(self, name, relation):
        self.name = name
        self.relation = relation


class _RoleTypes:

    UPSTREAM = _RoleType('Upstream', 'for')
    DOWNSTREAM = _RoleType('DOWNSTREAM', 'to')


class _Roles:

    CUSTOMER = _Role('Customer', _RoleTypes.DOWNSTREAM, 'a')
    SUPPLIER = _Role('Supplier', _RoleTypes.UPSTREAM, 'a')
    OPEN_HOST_SERVICE = _Role('Open Host Service', _RoleTypes.UPSTREAM, 'an')
    PUBLISHED_LANGUAGE = _Role('Published Language', _RoleTypes.UPSTREAM, 'a')
    ANTI_CORRUPTION_LAYER = _Role('Anti-corruption Layer', _RoleTypes.DOWNSTREAM, 'an')
    CONFORMIST = _Role('Conformist', _RoleTypes.DOWNSTREAM, 'a')


class _Participant:

    def __init__(self, bounded_context_name, role=None):
        self.bounded_context_name = bounded_context_name
        self.role = role

    def __repr__(self):
        return f'_Participant[name={self.bounded_context_name}, role={self.role}]'


class _ParticipantFactory:

    _SUPPLIER = ['Supplier', 'supplier', 'S', 's']
    _CUSTOMER = ['Customer', 'customer', 'C', 'c']
    _OPEN_HOST_SERVICE = ['Open Host Service', 'open host service', 'OpenHostService', 'OHS', 'ohs']
    _PUBLISHED_LANGUAGE = ['Published Language', 'published language', 'PublishedLanguage', 'PL', 'pl']
    _ANTI_CORRUPTION_LAYER = ['Anti-corruption Layer', 'anti-corruption layer', 'AnitCorruptionLayer', 'ACL', 'acl']
    _CONFORMIST = ['Conformist', 'conformist', 'CF', 'cf']

    @classmethod
    def create_participant(cls, participant):
        bounded_context_name = participant.get('name')
        _role = participant.get('role')
        if _role in cls._SUPPLIER:
            return _Participant(bounded_context_name, _Roles.SUPPLIER)
        if _role in cls._CUSTOMER:
            return _Participant(bounded_context_name, _Roles.CUSTOMER)
        if _role in cls._OPEN_HOST_SERVICE:
            return _Participant(bounded_context_name, _Roles.OPEN_HOST_SERVICE)
        if _role in cls._PUBLISHED_LANGUAGE:
            return _Participant(bounded_context_name, _Roles.PUBLISHED_LANGUAGE)
        if _role in cls._ANTI_CORRUPTION_LAYER:
            return _Participant(bounded_context_name, _Roles.ANTI_CORRUPTION_LAYER)
        if _role in cls._CONFORMIST:
            return _Participant(bounded_context_name, _Roles.CONFORMIST)


class _RelationshipBase:

    def __init__(self, rel_type):
        self.type = rel_type
        self.participants = []

    def describe(self, bounded_context_name):
        pass

    def get_other_participant(self, bounded_context_name):
        if bounded_context_name == self.participants[0].bounded_context_name:
            return self.participants[1]
        elif bounded_context_name == self.participants[1].bounded_context_name:
            return self.participants[0]

    def _get_participant(self, bounded_context_name):
        if bounded_context_name == self.participants[0].bounded_context_name:
            return self.participants[0]
        elif bounded_context_name == self.participants[1].bounded_context_name:
            return self.participants[1]


class _SymmetricRelationship(_RelationshipBase):

    def __init__(self, rel_type, participants, phrase):
        super().__init__(rel_type)
        self.phrase = phrase
        for bounded_context_name in participants:
            self.participants.append(_Participant(bounded_context_name))

    def describe(self, bounded_context_name):
        return self.phrase

    def __repr__(self):
        return f'_SymmetricRelationship[type={self.type}, participants={self.participants}, phrase={self.phrase}]'


class _PartnershipRelationship(_SymmetricRelationship):

    def __init__(self, participants):
        super().__init__(_RelationshipType.PARTNERSHIP, participants, 'Is in a Partnership with')


class _SharedKernelRelationship(_SymmetricRelationship):

    def __init__(self, participants):
        super().__init__(_RelationshipType.SHARED_KERNEL, participants, 'Has a Shared Kernel with')


class _SeparateWaysRelationship(_SymmetricRelationship):

    def __init__(self, participants):
        super().__init__(_RelationshipType.SEPARATE_WAYS, participants, 'Has gone its Separate Way from')


class _CustomerSupplierRelationship(_RelationshipBase):

    def __init__(self, participants):
        super().__init__(_RelationshipType.CUSTOMER_SUPPLIER)
        for p in participants:
            self.participants.append(_ParticipantFactory.create_participant(p))

    def describe(self, bounded_context_name):
        participant = self._get_participant(bounded_context_name)
        return participant.role.phrase()

    def __repr__(self):
        return f'_CustomerSupplierRelationship[participants={self.participants}]'


class RelationshipFactory:

    _PARTNERSHIP = ['partnership', 'Partnership', 'PRT', 'prt']
    _SHARED_KERNEL = ['shared kernel', 'Shared Kernel', 'SharedKernel', 'SK', 'sk']
    _CUSTOMER_SUPPLIER = ['Customer/Supplier', 'customer/supplier', 'Customer-Supplier', 'CustomerSupplier', 'CS', 'cs']
    _SEPARATE_WAYS = ['Separate Ways', 'separate ways', 'SeparateWays', 'SW', 'sw']

    @classmethod
    def create_relationship(cls, info):
        _type = info.get('type')
        participants = info.get('participants')
        if _type in cls._PARTNERSHIP:
            return _PartnershipRelationship(participants)
        if _type in cls._SHARED_KERNEL:
            return _SharedKernelRelationship(participants)
        if _type in cls._SEPARATE_WAYS:
            return _SeparateWaysRelationship(participants)
        if _type in cls._CUSTOMER_SUPPLIER:
            return _CustomerSupplierRelationship(participants)
