import argparse
import sys

from od.commands.generate import GenerateCommand
from od.commands.serve import ServeCommand


def main():
    argparser = argparse.ArgumentParser(prog='od',
                                        description='Tools for working with OpenDomain documents')
    argparser.add_argument('-v', '--verbose',
                           help='generate verbose output',
                           action='store_true',
                           dest='verbose')

    cmdparsers = argparser.add_subparsers(title='commands',
                                          required=True,
                                          dest='command',
                                          description='The following are valid commands:')

    """ Command 'generate' """
    genparser = cmdparsers.add_parser('generate',
                                      aliases=['gen'],
                                      description='Generate DDD documentation',
                                      help='generate documentation')
    genparser.add_argument('input',
                           metavar='<file>',
                           type=argparse.FileType('rb'),
                           help='Open Domain (*.yaml or *.yml) file to read')
    genparser.set_defaults(func=GenerateCommand.execute)

    """ Command 'serve' """
    srvparser = cmdparsers.add_parser('serve',
                                      aliases=['srv'],
                                      description='Serve DDD documentation',
                                      help='serve documentation')
    srvparser.add_argument('-d', '--debug',
                           help='run debug mode',
                           action='store_true',
                           dest='debug')
    srvparser.add_argument('input',
                           metavar='<file>',
                           # type=argparse.FileType('rb'),
                           help='Open Domain (*.yaml or *.yml) file to read')
    srvparser.set_defaults(func=ServeCommand.execute)

    args = argparser.parse_args()
    try:
        args.func(args)
    except BrokenPipeError:
        pass
    sys.stderr.close()
