class Attribute:

    def __init__(self, info, is_identifier=False):
        self.name = info.get('name')
        self.type = info.get('type')
        self.is_identifier = is_identifier

    def __repr__(self):
        return f'Attribute[{self.name}::{self.type}]'
