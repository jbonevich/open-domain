from flask import Flask, render_template
from od.flask import views
from od.flask.datastore import ds


class FlaskApp:

    def __init__(self, host, port, domain, resource_dir, debug=False):
        self.host = host
        self.port = port
        self.domain = domain
        self.resource_dir = resource_dir

        app = Flask(__name__)
        app.config['DEBUG'] = debug
        self.register_errorhandlers(app)
        self.register_blueprints(app)
        self.register_extensions(app)
        self.app = app

    def register_errorhandlers(self, app):
        def render_error(error):
            """Render error template."""
            # If a HTTPException, pull the `code` attribute; default to 500
            error_code = getattr(error, "code", 500)
            return render_template(f'{error_code}.html', domain=self.domain), error_code

        for errcode in [404, 500]:
            app.errorhandler(errcode)(render_error)

    def register_blueprints(self, app):
        app.register_blueprint(views.blueprint)

    def register_extensions(self, app):
        ds.init_app(app, self.domain, self.resource_dir)

    def run(self):
        self.app.run(host=self.host, port=self.port)
