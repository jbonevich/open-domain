import yaml

from od.model.domain import Domain


class GenerateCommand:

    @staticmethod
    def execute(args):
        stream = args.input
        data = yaml.load(stream, Loader=yaml.Loader)
        domain = Domain(data.get('domain'))
        print(domain)
        print(domain.core_domain)
        print(domain.subdomains)
