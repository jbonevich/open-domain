from od.model.model import ConceptualBase, ModelType


class Event(ConceptualBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.DOMAIN_EVENT, module, supports_attributes=True)

    def __repr__(self):
        return f'Event[{self.name}]'
