from od.model.model import ModularBase, ModelType


class Repository(ModularBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.REPOSITORY, module)

    def __repr__(self):
        return f'Repository[{self.name}]'
