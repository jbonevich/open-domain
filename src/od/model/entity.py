from od.model.attribute import Attribute
from od.model.model import ConceptualBase, ModelType


class Entity(ConceptualBase):

    def __init__(self, info, module):
        super().__init__(info, ModelType.ENTITY, module, supports_attributes=True)

        self.id = None
        _id = info.get('identifier')
        if _id:
            self.id = Attribute(_id, is_identifier=True)

    def __repr__(self):
        return f'Entity[{self.name}]'
