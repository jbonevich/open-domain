from od.model.module import Module


class _ApplicableSubdomains:

    def __init__(self, subdomain, root_domain):
        self.subdomains = []
        if isinstance(subdomain, list):
            for name in subdomain:
                sub = root_domain.get_subdomain(name)
                self.subdomains.append(sub)
        else:
            sub = root_domain.get_subdomain(subdomain)
            self.subdomains.append(sub)

    def __iter__(self):
        return iter(self.subdomains)

    @property
    def first(self):
        return self.subdomains[0]

    def get_domain_concept(self, term):
        for sub in self.subdomains:
            concept = sub.get_domain_concept(term)
            if concept:
                return concept
            # TODO: what if there are multiple concepts that match? require disambiguation?

    @property
    def has_multiple(self):
        return len(self.subdomains) > 1


class _Relationship:

    def __init__(self, bounded_context, relationship):
        self.bounded_context = bounded_context
        self.relationship = relationship

    @property
    def description(self):
        return self.relationship.describe(self.bounded_context.name)

    @property
    def other_participant(self):
        return self.relationship.get_other_participant(self.bounded_context.name).bounded_context_name


class BoundedContext:

    def __init__(self, info, root_domain):
        self.name = info.get('name')
        self.scm_url = info.get('scm_url')
        self.ci_url = info.get('ci_url')
        self.base_package = info.get('base_package')
        # TODO: develop a default base package, something like name or short_name + 'domain.model'

        team_name = info.get('team')
        self.team = root_domain.get_team(team_name)

        self.subdomains = _ApplicableSubdomains(info.get_raw('subdomain'), root_domain)

        self.model_objects = {}

        self.modules = []
        modules = info.get_all('modules')
        for mod in modules:
            self.modules.append(Module(mod, self, self.base_package))

        self.relationships = []

    def get_domain_concept(self, term):
        return self.subdomains.get_domain_concept(term)

    def get_model_object(self, name):
        return self.model_objects[name]

    def add_model_object(self, model):
        self.model_objects[model.name] = model

    def add_relationship(self, rel):
        self.relationships.append(_Relationship(self, rel))

    def add_sub_module(self, module):
        self.modules.append(module)

    def __repr__(self):
        return f'BoundedContext[name={self.name}]'
