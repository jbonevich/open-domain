def _wrap(v):
    if isinstance(v, dict):
        return Dict(v)
    if isinstance(v, list):
        return _ItemIterator(v)
    return v


class Dict:

    def __init__(self, dictionary):
        self.dict = dictionary

    def get(self, key):
        return _wrap(self.dict.get(key))

    def get_raw(self, key):
        return self.dict.get(key)

    def get_all(self, key):
        values = self.get(key)
        if isinstance(values, _ItemIterator):
            return values
        if not values:
            return _ItemIterator(())
        return _ItemIterator([values])


class _ItemIterator:

    def __init__(self, items):
        self._itr = iter(items)
        self.length = len(items)

    def __iter__(self):
        return self

    def __next__(self):
        return _wrap(next(self._itr))

    def __len__(self):
        return self.length
